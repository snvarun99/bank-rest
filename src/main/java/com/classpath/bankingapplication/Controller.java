package com.classpath.bankingapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    AccountService accountService;

    @PostMapping
    public String saveAccountDetails(@RequestBody Account account){

        accountService.save(account);



        return "Account saved";
    }



}
