package com.classpath.bankingapplication;

public enum Status {
    ACTIVE,
    INACTICE,
    CLOSED
}