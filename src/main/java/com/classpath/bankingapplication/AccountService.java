package com.classpath.bankingapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired
    private BankRepo bankRepo;


    public void save(Account account){
        bankRepo.save(account);
    }

}
