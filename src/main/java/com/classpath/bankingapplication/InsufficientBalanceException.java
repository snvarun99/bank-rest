package com.classpath.bankingapplication;

public class InsufficientBalanceException extends Exception{

    public InsufficientBalanceException(String message){
        super(message);
    }

    @Override
    public String getMessage(){
        return super.getMessage();
    }

}