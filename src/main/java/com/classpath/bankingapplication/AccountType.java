package com.classpath.bankingapplication;

public enum AccountType {
    SAVINGS,
    CURRENT
}
